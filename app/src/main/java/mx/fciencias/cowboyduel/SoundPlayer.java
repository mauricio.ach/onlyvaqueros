package mx.fciencias.cowboyduel;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import org.jetbrains.annotations.NotNull;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class SoundPlayer extends JobIntentService {

    public static final String ACTION_FIRE = "mx.fciencias.cowboyduel.action.FIRE";
    public static final byte JOB_ID = 0;

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        String action = intent.getAction();
        if(action == null) action = "";
        switch (action) {
            case ACTION_FIRE :
                MediaPlayer soundPlayer =
                        MediaPlayer.create(this,R.raw.fire);
                soundPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Log.d(SoundPlayer.class.getSimpleName(),"Freeing sound related resources");
                        mp.release();
                    }
                });
                soundPlayer.start();
                break;
            default:
                Log.d(SoundPlayer.class.getSimpleName(), "Unrecognized action: " +action);
        }
    }

    public static void enqueueWork (@NonNull Context context,@NonNull Intent work){
        enqueueWork(context, SoundPlayer.class, JOB_ID,work);
    }
}
