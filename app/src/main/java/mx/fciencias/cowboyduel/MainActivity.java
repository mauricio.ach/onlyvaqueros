package mx.fciencias.cowboyduel;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

/**
 * @author Mauricio Araujo Chavez
 */
public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private ImageView gunView;
    @Nullable
    private SensorManager sensorManager;
    @Nullable
    private Sensor stepDetectorSensor;
    private byte steps;
    public static final byte SECONDS_TO_COUNT = 3;
    private AsyncCounter asyncCounter;
    private Button startButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gunView = findViewById(R.id.gun_iv);
        startButton=findViewById(R.id.start_button);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if(sensorManager == null) return;
        stepDetectorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        if(stepDetectorSensor == null)
            sensorManager = null;
    }

    @Override
    protected void onPause() {
        if(sensorManager != null){
            sensorManager.unregisterListener(this);
        }
        super.onPause();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus){
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
            );
        }
    }

    public void finalCountdown(View startButton) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        startButton.setVisibility(View.INVISIBLE);
        checkStepSensor();
    }

    public void fire(View gun){
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        SoundPlayer.enqueueWork(this,new Intent(SoundPlayer.ACTION_FIRE));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                init();
            }

        }, 3000);
    }

    public void checkStepSensor(){
        if(sensorManager == null){
            startTimer();
            return;
        }
        sensorManager.registerListener(this,stepDetectorSensor,SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void startTimer() {
        if(asyncCounter != null &&
                !asyncCounter.getStatus().equals(AsyncTask.Status.FINISHED)) {
            asyncCounter.cancel(true);
        }
        asyncCounter = new AsyncCounter();
        asyncCounter.execute(gunView);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        steps++;
        if(steps >= SECONDS_TO_COUNT) {
            sensorManager.unregisterListener(this);
            gunView.setVisibility(View.VISIBLE);
            steps = 0;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    private void init() {
        startButton.setVisibility(View.VISIBLE);
        gunView.setVisibility(View.INVISIBLE);
    }

    private void killCounter() {
        if(sensorManager != null)
            sensorManager.unregisterListener(this);
        else if(asyncCounter != null &&
                !asyncCounter.getStatus().equals(
                        AsyncTask.Status.FINISHED))
            asyncCounter.cancel(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }
}
