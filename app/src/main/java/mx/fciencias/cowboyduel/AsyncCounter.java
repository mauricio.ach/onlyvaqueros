package mx.fciencias.cowboyduel;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class AsyncCounter extends AsyncTask<ImageView, Byte, ImageView> {

    @Override
    protected ImageView doInBackground(ImageView[] images) {
        byte counter = 0;
        while (counter < MainActivity.SECONDS_TO_COUNT) {
            if(isCancelled()) return images[0];
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Log.d(
                        AsyncCounter.class.getSimpleName(),
                        "The counter was interrupted!");
                return images[0];
            }
            counter++;
            publishProgress(counter);
        }
        return images[0];
    }

    @Override
    protected void onProgressUpdate(Byte... values) {
        Log.d(AsyncCounter.class.getSimpleName(),"Counted to" + values[0]);
    }

    @Override
    protected void onCancelled(ImageView image) {
        if(image != null)
            image.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onPostExecute(ImageView imageView) {
        imageView.setVisibility(View.VISIBLE);
    }
}
